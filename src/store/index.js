import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function getPhoto(array) {
  var obj = array.find(attachment => attachment.type === 'photo');

  return obj ? obj.photo.photo_604 : null
}

export default new Vuex.Store({
  state: {
    newsList: [],
  },
  mutations: {
    getNews(state) {

      // fetch('https://thingproxy.freeboard.io/fetch/http://lenta.ch/api/posts', {
        fetch('https://cors-anywhere.herokuapp.com/http://lenta.ch/api/posts', {
      })
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          let currentNewsList = data.posts.splice(0, 5);
          state.newsList = currentNewsList.map(function (newsItem) {
            return {
              'id': newsItem._id,
              'createdAt': newsItem.createdAt,
              'changedAt': null,
              'text': newsItem.text,
              'photo': getPhoto(newsItem.attachments),
              'link': newsItem.attachments
                .filter(attachment => attachment.type === 'link')
                .map(attachment => attachment.link),
            }

          });

        });
    },

    changeNewsItem(state, data) {
      const curNewsItem = state.newsList.find(newsItem => newsItem.id === data.id);

      for (let key in data) {
        if (data[key] === null || data[key] === undefined || data[key] === '') {
          delete data[key];
        }
      }
      
      Object.keys(data).forEach(param => {
        curNewsItem[param] = data[param]
      })
    }
  }
})
